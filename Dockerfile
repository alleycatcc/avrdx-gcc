FROM debian:trixie-20231009-slim

RUN apt update

RUN apt install --no-install-recommends -y \
  ca-certificates \
  git gcc make automake \
  python3 curl \
  gcc-avr

RUN git clone https://github.com/avrdudes/avr-libc/

COPY build.sh /build.sh

ARG VERSION
RUN bash /build.sh $VERSION
