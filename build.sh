#!/usr/bin/env bash

set -eu
set -o pipefail

bindir=$(realpath "$(dirname "$0")")

cmd () {
  echo "٭ $@"
  "$@"
}

cwd () {
  local dir=$1; shift
  echo '[cwd] '"$dir"
  cd "$dir" && cmd "$@" && cd "$OLDPWD"
}

version=$1

rawrepourl=https://raw.githubusercontent.com/epccs/AVR-Dx_DFP/master/gcc/dev
gccdevicespecsdir=/usr/lib/gcc/avr/"$version"/device-specs

# --- the devices available at the AVR-Dx_DFP repo
devices=(
  avr128da28
  avr128da32
  avr128da48
  avr128da64
  avr128db28
  avr128db32
  avr128db48
  avr128db64
  avr32da28
  avr32da32
  avr32da48
  avr32db28
  avr32db32
  avr32db48
  avr64da28
  avr64da32
  avr64da48
  avr64da64
  avr64db28
  avr64db32
  avr64db48
  avr64db64
)

get-spec() {
  local device=$1
  local spec=specs-"$device"
  cmd curl -sSLo \
    "$gccdevicespecsdir"/"$spec" \
    "$rawrepourl"/"$device"/device-specs/"$spec"
}

get-specs() {
  local device
  for device in "${devices[@]}"; do
    get-spec "$device"
  done
}

build() {
  cmd sed -i 's,#!/usr/bin/env python,#!/usr/bin/env python3,' devtools/mlib-gen.py
  cmd ./bootstrap
  cmd ./configure --build=$(./config.guess) --host=avr
  cmd make -j
}

go() {
  get-specs
  cwd avr-libc build
}

cwd "$bindir" go |& tee /build-log
