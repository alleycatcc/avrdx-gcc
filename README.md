A Docker image for building for AVRDx microcontrollers from Microchip using gcc.

It works by patching the gcc-avr package from Debian with the missing
device-specs for the Dx family of chips, then building avrdudes/avr-libc
from source.

Build the image with

    # --- change VERSION as necesary
    docker build --build-arg=VERSION=14.2.0 -t alleycatcc/avrdx-gcc .

Copy the tools out of the image using `docker cp`, or use `docker run --it alleycatcc/avrdx-gcc bash` with the `-v` option to mount a folder and run the compiler from inside the image.

Steps to copy:

    id=$(docker create avrdx-gcc:latest)
    sudo mkdir /usr/local/lib/gcc
    # --- this will be almost identical to the version you get from apt,
    # except with extra files in the device-specs/ directory.
    sudo docker cp "$id":/usr/lib/gcc/avr /usr/local/lib/gcc/avr
    # --- consider this an enhanced override for avrdudes/avr-libc
    sudo docker cp "$id":/avr-libc /usr/local/lib

Then, compiling and deploying for avr128db64, for example, may look something like:

    device=avr128db64
    gcc_avr_dir=/usr/local/lib/gcc/avr/7.3.0
    avr_libc_dir=/usr/local/lib/avr-libc
    avr-gcc -B "$gcc_avr_dir" -B "$avr_libc_dir"/avr/devices/"$device" -B "$avr_libc_dir"/avr/lib/avrxmega3 -I "$avr_libc_dir"/include -Os -mmcu="$device" -Wmain -DF_CPU=4000000 <source-files> -oa.out
    avr-objcopy -O ihex a.out a.hex
    pymcuprog write --erase -f a.hex
