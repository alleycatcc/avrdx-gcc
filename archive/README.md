The specs are from the
https://github.com/epccs/AVR-Dx_DFP repository, which in turn gets them from
Microchip's packs.

They're archived here in case that repo goes offline.
